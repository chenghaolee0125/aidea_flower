import os
import numpy as np
import pandas as pd
from setting import *
from utils import *

for i in range(len(file_names_)):
    print(file_names_[i])
    df = pd.read_csv(os.path.join(DATA_ROOT_,file_names_[i])+'.csv')

    cleasing_commas_cols = ['trade_amount', 'price_max', 'leftover']
    for col in cleasing_commas_cols:
        df[col] = remove_commas(df[col])
        df[col] = invert_to_int(df[col])
    cleasing_dash_cols = ['price_compare','trade_compare']
    for col in cleasing_dash_cols:
        df[col] = remove_dash(df[col])
        df[col] = invert_to_int(df[col])

    df['date'] = replace_year(df['date'])

    timestamp_flower = pd.to_datetime(df['date'], format='%Y/%m/%d').map(datetime.datetime.timestamp)
    df['weekday'] = pd.to_datetime(df['date'], format='%Y/%m/%d').dt.dayofweek


    df = df.drop(columns=['place','product'])

    weather_column = ['ObsTime','Temperature','T Max','T Min','RH','RHMin','Precp']
    df_weather = pd.read_csv(WEATHER_ROOT_)
    df_weather = df_weather[weather_column]

    df_weather = cleanse_weather_by_duplicate(df_weather)
    df_weather[weather_column[1:]] = df_weather[weather_column[1:]].astype(float)
    timestamp_weather = pd.to_datetime(df_weather['ObsTime'], format='%Y/%m/%d').map(datetime.datetime.timestamp)
    # print(timestamp_weather.head())
    # print(df_weather.info())
    # data_visualize(df_weather)


    flower_idx = 0
    weather_idx = 0
    # print(df_weather.iloc[[0,1]])
    weather_parse_idx = list()
    while flower_idx < len(timestamp_flower):
        if timestamp_flower[flower_idx] == timestamp_weather[weather_idx]:
            weather_parse_idx.append(weather_idx)
            flower_idx += 1
            weather_idx += 1
        elif timestamp_flower[flower_idx] > timestamp_weather[weather_idx]:
            weather_idx += 1
        elif timestamp_flower[flower_idx] < timestamp_weather[weather_idx]:
            flower_idx += 1

    df_weather_parse = df_weather.iloc[weather_parse_idx]
    df_weather_parse = df_weather_parse.reset_index(drop=True)
    # print(df_weather_parse.head())
    # print(df.head())
    df = pd.concat([df, df_weather_parse],axis=1)
    df = df.drop(columns=['ObsTime'])
    # print(df.head())
    # data_cor_heatmap(df)

    output_path = os.path.join(ClEANSE_DATA_ROOT_,file_names_[i])+'.csv'
    df.to_csv(output_path, index = False)