import os
import datetime

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf

from utils import *
from Model import WindowGenerator, Baseline, compile_and_fit, ResidualWrapper, FeedBack

# mpl.rcParams['figure.figsize'] = (8, 6)
# mpl.rcParams['axes.grid'] = False
for file_name in setting.file_names:
    print(file_name)
    df = pd.read_csv(os.path.join(setting.DATA_ROOT_,file_name)+'.csv')
    # print(df.head())

    day = 24*60*60
    month = 29.5305 * day

    cleasing_commas_cols = ['trade_amount', 'price_max', 'leftover']
    for col in cleasing_commas_cols:
        df[col] = remove_commas(df[col])
        df[col] = invert_to_int(df[col])
    cleasing_dash_cols = ['price_compare','trade_compare']
    for col in cleasing_dash_cols:
        df[col] = remove_dash(df[col])
        df[col] = invert_to_int(df[col])

    # data_visualize(df)

    df_date = df.pop('date')
    df_date = replace_year(df_date)
    date_time = pd.to_datetime(df_date, format='%Y/%m/%d')
    timestamp_s = date_time.map(datetime.datetime.timestamp)
    df_date = date_to_period(timestamp_s)
    df['month_sin'] = np.sin(timestamp_s * (2 * np.pi / month))
    df['month_cos'] = np.cos(timestamp_s * (2 * np.pi / month))
    df = df.drop(columns=['place','product'])
    df = pd.concat([df,df_date],axis=1)


    # print()
    # print(df.head())
    #    price_max  price_high  price_mid  price_low  price_avg  price_compare  trade_amount  trade_compare  leftover  month_sin  month_cos  year_period
    # 0        172         167        135         82      130.0            -23           764            105         0   0.543962   0.839110         33.0
    # 1        191         181        129         94      132.0              0           884              0         0   0.841843   0.539722         31.0
    # 2        198         174        127         97      130.0             -2           650            -26         0   0.936832   0.349781         30.0
    # 3        210         208        185        134      179.0             38           307            -53         0   0.989568   0.144065         29.0
    # 4        202         190        160        121      158.0            -12           596             94         0   0.997675  -0.068149         28.0
    ######################################################################

    column_indices = {name: i for i, name in enumerate(df.columns)}

    n = len(df)
    train_df = df[int(n*0.3):-13]
    val_df = df[int(n*0.1):int(n*0.3)]
    test_df = df[0:int(n*0.1)]
    # print('test_df before normalize', train_df.iloc[-13:])

    num_features = df.shape[1]

    train_mean = train_df.mean()
    train_std = train_df.std()

    train_df = (train_df - train_mean) / train_std
    val_df = (val_df - train_mean) / train_std
    test_df = (test_df - train_mean) / train_std
    # print('train_std',train_std)
    # print('train_mean',train_mean)
    # print('test_df after normalize', train_df.iloc[-13:])
    
    # print(train_df)
    OUT_STEPS = 13
    IN_STEPS = 13
    # ##############################################################################
    # # w1 = WindowGenerator(input_width=24, label_width=1, shift=24, 
    # #                     train_df=train_df, val_df=val_df, test_df=test_df,
    # #                     label_columns=['trade_amount'])
    # # print(w1)

    # w2 = WindowGenerator(input_width=6, label_width=1, shift=1,
    #                     train_df=train_df, val_df=val_df, test_df=test_df,
    #                      label_columns=['trade_amount'])
    # # print(w2)
    # #########################################################################
    # # Stack three slices, the length of the total window:
    # example_window = tf.stack([np.array(train_df[:w2.total_window_size]),
    #                            np.array(train_df[100:100+w2.total_window_size]),
    #                            np.array(train_df[200:200+w2.total_window_size])])


    # example_inputs, example_labels = w2.split_window(example_window)

    # # print('All shapes are: (batch, time, features)')
    # # print(f'Window shape: {example_window.shape}')
    # # print(f'Inputs shape: {example_inputs.shape}')
    # # print(f'labels shape: {example_labels.shape}')

    # #########################################################################
    # # w2.example = example_inputs, example_labels
    # # w2.plot()
    # # print(w2.train.element_spec)
    # # for example_inputs, example_labels in w2.train.take(1):
    # #   print(f'Inputs shape (batch, time, features): {example_inputs.shape}')
    # #   print(f'Labels shape (batch, time, features): {example_labels.shape}')

    # single_step_window = WindowGenerator(
    #     input_width=1, label_width=1, shift=1,
    #     train_df=train_df, val_df=val_df, test_df=test_df,
    #     label_columns=['trade_amount'])
    # # print(single_step_window)

    # # for example_inputs, example_labels in single_step_window.train.take(1):
    # #   print(f'Inputs shape (batch, time, features): {example_inputs.shape}')
    # #   print(f'Labels shape (batch, time, features): {example_labels.shape}')

    # # baseline = Baseline(label_index=column_indices['trade_amount'])

    # # baseline.compile(loss=tf.losses.MeanSquaredError(),
    # #                  metrics=[tf.metrics.MeanAbsoluteError()])

    val_performance = {}
    performance = {}
    # # val_performance['Baseline'] = baseline.evaluate(single_step_window.val)
    # # performance['Baseline'] = baseline.evaluate(single_step_window.test, verbose=0)
    # # #####################################################################################
    wide_window = WindowGenerator(
        input_width=IN_STEPS, label_width=OUT_STEPS, shift=OUT_STEPS,
        train_df=train_df, val_df=val_df, test_df=test_df,
        label_columns=['trade_amount'])

    # # # wide_window.plot(baseline)

    linear = tf.keras.Sequential([
        tf.keras.layers.Dense(units=OUT_STEPS)
    ])

    history = compile_and_fit(linear, wide_window)

    val_performance['Linear'] = linear.evaluate(wide_window.val)
    performance['Linear'] = linear.evaluate(wide_window.test, verbose=0)

    # # # wide_window.plot(linear)
    # # #########################################################################################
    # # feature selection

    # # plt.bar(x = range(len(train_df.columns)),
    # #         height=linear.layers[0].kernel[:,0].numpy())
    # # axis = plt.gca()
    # # axis.set_xticks(range(len(train_df.columns)))
    # # _ = axis.set_xticklabels(train_df.columns, rotation=90)
    # # plt.show()
    # # #######################################################################################
    # dense = tf.keras.Sequential([
    #     tf.keras.layers.Dense(units=64, activation='relu'),
    #     tf.keras.layers.Dense(units=64, activation='relu'),
    #     tf.keras.layers.Dense(units=1)
    # ])

    # # history = compile_and_fit(dense, single_step_window)

    # # val_performance['Dense'] = dense.evaluate(single_step_window.val)
    # # performance['Dense'] = dense.evaluate(single_step_window.test, verbose=0)

    # # # wide_window.plot(dense)
    # #######################################################################################
    # CONV_WIDTH = 3
    # conv_window = WindowGenerator(
    #     input_width=CONV_WIDTH,
    #     label_width=1,
    #     shift=1,
    #     train_df=train_df, val_df=val_df, test_df=test_df,
    #     label_columns=['trade_amount'])

    # # # print(conv_window)

    # # # conv_window.plot()
    # # # plt.title("Given 3h as input, predict 1h into the future.")

    # # ####################################################################################
    # multi_step_dense = tf.keras.Sequential([
    #     # Shape: (time, features) => (time*features)
    #     tf.keras.layers.Flatten(),
    #     tf.keras.layers.Dense(units=32, activation='relu'),
    #     tf.keras.layers.Dense(units=32, activation='relu'),
    #     tf.keras.layers.Dense(units=1),
    #     # Add back the time dimension.
    #     # Shape: (outputs) => (1, outputs)
    #     tf.keras.layers.Reshape([1, -1]),
    # ])
    # # history = compile_and_fit(multi_step_dense, conv_window)

    # # val_performance['Multi step dense'] = multi_step_dense.evaluate(conv_window.val)
    # # performance['Multi step dense'] = multi_step_dense.evaluate(conv_window.test, verbose=0)

    # # ######################################################################
    # conv_model = tf.keras.Sequential([
    #     tf.keras.layers.Conv1D(filters=32,
    #                            kernel_size=(CONV_WIDTH,),
    #                            activation='relu'),
    #     tf.keras.layers.Dense(units=32, activation='relu'),
    #     tf.keras.layers.Dense(units=1),
    # ])
    # # # print("Conv model on `conv_window`")
    # # # print('Input shape:', conv_window.example[0].shape)
    # # # print('Output shape:', conv_model(conv_window.example[0]).shape)

    # # history = compile_and_fit(conv_model, conv_window)

    # # val_performance['Conv'] = conv_model.evaluate(conv_window.val)
    # # performance['Conv'] = conv_model.evaluate(conv_window.test, verbose=0)
    # # # print("Wide window")
    # # # print('Input shape:', wide_window.example[0].shape)
    # # # print('Labels shape:', wide_window.example[1].shape)
    # # # print('Output shape:', conv_model(wide_window.example[0]).shape)
    # # ###########################################################################
    # LABEL_WIDTH = 24
    # INPUT_WIDTH = LABEL_WIDTH + (CONV_WIDTH - 1)
    # wide_conv_window = WindowGenerator(
    #     input_width=INPUT_WIDTH,
    #     label_width=LABEL_WIDTH,
    #     shift=1,
    #     train_df=train_df, val_df=val_df, test_df=test_df,
    #     label_columns=['trade_amount'])

    # # print(wide_conv_window)

    # # ######################################################################
    # lstm_model = tf.keras.models.Sequential([
    #     # Shape [batch, time, features] => [batch, time, lstm_units]
    #     tf.keras.layers.LSTM(32, return_sequences=True),
    #     # Shape => [batch, time, features]
    #     tf.keras.layers.Dense(units=1)
    # ])
    # # # print('Input shape:', wide_window.example[0].shape)
    # # # print('Output shape:', lstm_model(wide_window.example[0]).shape)

    # # history = compile_and_fit(lstm_model, wide_window)

    # # val_performance['LSTM'] = lstm_model.evaluate(wide_window.val)
    # # performance['LSTM'] = lstm_model.evaluate(wide_window.test, verbose=0)
    # # # wide_window.plot(lstm_model)
    # # ########################################################################
    # # x = np.arange(len(performance))
    # # width = 0.3
    # # metric_name = 'mean_absolute_error'
    # # metric_index = lstm_model.metrics_names.index('mean_absolute_error')
    # # val_mae = [v[metric_index] for v in val_performance.values()]
    # # test_mae = [v[metric_index] for v in performance.values()]

    # # plt.ylabel('mean_absolute_error [trade_amount, normalized]')
    # # plt.bar(x - 0.17, val_mae, width, label='Validation')
    # # plt.bar(x + 0.17, test_mae, width, label='Test')
    # # plt.xticks(ticks=x, labels=performance.keys(),
    # #            rotation=45)
    # # _ = plt.legend()
    # # plt.show()

    # ###################################################################
    # single_step_window = WindowGenerator(
    #     # `WindowGenerator` returns all features as labels if you 
    #     # don't set the `label_columns` argument.
    #     input_width=1, label_width=1, shift=1,
    #     train_df=train_df, val_df=val_df, test_df=test_df)

    # wide_window = WindowGenerator(
    #     input_width=24, label_width=24, shift=1,
    #     train_df=train_df, val_df=val_df, test_df=test_df)


    # # for example_inputs, example_labels in wide_window.train.take(1):
    # #   print(f'Inputs shape (batch, time, features): {example_inputs.shape}')
    # #   print(f'Labels shape (batch, time, features): {example_labels.shape}')
    # baseline = Baseline()
    # baseline.compile(loss=tf.losses.MeanSquaredError(),
    #                  metrics=[tf.metrics.MeanAbsoluteError()])

    # val_performance = {}
    # performance = {}
    # val_performance['Baseline'] = baseline.evaluate(wide_window.val)
    # performance['Baseline'] = baseline.evaluate(wide_window.test, verbose=0)

    # dense = tf.keras.Sequential([
    #     tf.keras.layers.Dense(units=64, activation='relu'),
    #     tf.keras.layers.Dense(units=64, activation='relu'),
    #     tf.keras.layers.Dense(units=num_features)
    # ])

    # history = compile_and_fit(dense, single_step_window)
    # val_performance['Dense'] = dense.evaluate(single_step_window.val)
    # performance['Dense'] = dense.evaluate(single_step_window.test, verbose=0)

    # lstm_model = tf.keras.models.Sequential([
    #     # Shape [batch, time, features] => [batch, time, lstm_units]
    #     tf.keras.layers.LSTM(32, return_sequences=True),
    #     # Shape => [batch, time, features]
    #     tf.keras.layers.Dense(units=num_features)
    # ])
    # history = compile_and_fit(lstm_model, wide_window)
    # val_performance['LSTM'] = lstm_model.evaluate( wide_window.val)
    # performance['LSTM'] = lstm_model.evaluate( wide_window.test, verbose=0)

    # residual_lstm = ResidualWrapper(
    #     tf.keras.Sequential([
    #     tf.keras.layers.LSTM(32, return_sequences=True),
    #     tf.keras.layers.Dense(
    #         num_features,
    #         # The predicted deltas should start small
    #         # So initialize the output layer with zeros
    #         kernel_initializer=tf.initializers.zeros)
    # ]))

    # history = compile_and_fit(residual_lstm, wide_window)

    # val_performance['Residual LSTM'] = residual_lstm.evaluate(wide_window.val)
    # performance['Residual LSTM'] = residual_lstm.evaluate(wide_window.test, verbose=0)

    # # x = np.arange(len(performance))
    # # width = 0.3

    # # metric_name = 'mean_absolute_error'
    # # metric_index = lstm_model.metrics_names.index('mean_absolute_error')
    # # val_mae = [v[metric_index] for v in val_performance.values()]
    # # test_mae = [v[metric_index] for v in performance.values()]

    # # plt.bar(x - 0.17, val_mae, width, label='Validation')
    # # plt.bar(x + 0.17, test_mae, width, label='Test')
    # # plt.xticks(ticks=x, labels=performance.keys(),
    # #            rotation=45)
    # # plt.ylabel('MAE (average over all outputs)')
    # # _ = plt.legend()
    # # plt.show()
    while False:
        # ###############################################################

        multi_window = WindowGenerator(
            input_width=IN_STEPS,
            label_width=OUT_STEPS,
            shift=OUT_STEPS,
            train_df=train_df, val_df=val_df, test_df=test_df)
        # for example_inputs, example_labels in multi_window.test.take(1):
        #   print(f'Inputs shape (batch, time, features): {example_inputs.shape}')
        #   print(f'Labels shape (batch, time, features): {example_labels.shape}')
        # multi_window.plot()

        # ###############################################################
        # class MultiStepLastBaseline(tf.keras.Model):
        #   def call(self, inputs):
        #     return tf.tile(inputs[:, -1:, :], [1, OUT_STEPS, 1])
        # last_baseline = MultiStepLastBaseline()
        # last_baseline.compile(loss=tf.losses.MeanSquaredError(),
        #                       metrics=[tf.metrics.MeanAbsoluteError()])

        multi_val_performance = {}
        multi_performance = {}

        # multi_val_performance['Last'] = last_baseline.evaluate(multi_window.val)
        # multi_performance['Last'] = last_baseline.evaluate(multi_window.test, verbose=0)
        # # multi_window.plot(last_baseline)
        # #################################################################
        # class RepeatBaseline(tf.keras.Model):
        #   def call(self, inputs):
        #     return inputs

        # repeat_baseline = RepeatBaseline()
        # repeat_baseline.compile(loss=tf.losses.MeanSquaredError(),
        #                         metrics=[tf.metrics.MeanAbsoluteError()])

        # multi_val_performance['Repeat'] = repeat_baseline.evaluate(multi_window.val)
        # multi_performance['Repeat'] = repeat_baseline.evaluate(multi_window.test, verbose=0)
        # # multi_window.plot(repeat_baseline)

        # #################################################################
        # multi_linear_model = tf.keras.Sequential([
        #     # Take the last time-step.
        #     # Shape [batch, time, features] => [batch, 1, features]
        #     tf.keras.layers.Lambda(lambda x: x[:, -1:, :]),
        #     # Shape => [batch, 1, out_steps*features]
        #     tf.keras.layers.Dense(OUT_STEPS*num_features,
        #                         kernel_initializer=tf.initializers.zeros),
        #     # Shape => [batch, out_steps, features]
        #     tf.keras.layers.Reshape([OUT_STEPS, num_features])
        # ])

        # history = compile_and_fit(multi_linear_model, multi_window)

        # multi_val_performance['Linear'] = multi_linear_model.evaluate(multi_window.val)
        # multi_performance['Linear'] = multi_linear_model.evaluate(multi_window.test, verbose=0)
        # multi_window.plot(multi_linear_model)

        ##########################################################################
        # multi_dense_model = tf.keras.Sequential([
        #     # Take the last time step.
        #     # Shape [batch, time, features] => [batch, 1, features]
        #     tf.keras.layers.Lambda(lambda x: x[:, -1:, :]),
        #     # Shape => [batch, 1, dense_units]
        #     tf.keras.layers.Dense(128, activation='relu'),
        #     # Shape => [batch, out_steps*features]
        #     tf.keras.layers.Dense(OUT_STEPS*num_features,
        #                         kernel_initializer=tf.initializers.zeros),
        #     # Shape => [batch, out_steps, features]
        #     tf.keras.layers.Reshape([OUT_STEPS, num_features])
        # ])

        # history = compile_and_fit(multi_dense_model, multi_window)

        # multi_val_performance['Dense'] = multi_dense_model.evaluate(multi_window.val)
        # multi_performance['Dense'] = multi_dense_model.evaluate(multi_window.test, verbose=0)
        # multi_window.plot(multi_dense_model)
        ##########################################################################
        # multi_lstm_model = tf.keras.Sequential([
        #     # Shape [batch, time, features] => [batch, lstm_units]
        #     # Adding more `lstm_units` just overfits more quickly.
        #     tf.keras.layers.LSTM(64, return_sequences=False),
        #     # Shape => [batch, out_steps*features]
        #     tf.keras.layers.Dense(OUT_STEPS*num_features,
        #                         kernel_initializer=tf.initializers.zeros),
        #     # Shape => [batch, out_steps, features]
        #     tf.keras.layers.Reshape([OUT_STEPS, num_features])
        # ])

        # history = compile_and_fit(multi_lstm_model, multi_window)

        # multi_val_performance['LSTM'] = multi_lstm_model.evaluate(multi_window.val)
        # multi_performance['LSTM'] = multi_lstm_model.evaluate(multi_window.test, verbose=0)
        # multi_window.plot(multi_lstm_model)
        ##########################################################################
        # feedback_model = FeedBack(units=8, out_steps=OUT_STEPS)
        # prediction, state = feedback_model.warmup(multi_window.example[0])
        # # print('Output shape (batch, time, features): ', feedback_model(multi_window.example[0]).shape)

        # history = compile_and_fit(feedback_model, multi_window)

        # multi_val_performance['AR LSTM'] = feedback_model.evaluate(multi_window.val)
        # multi_performance['AR LSTM'] = feedback_model.evaluate(multi_window.test, verbose=0)
        # multi_window.plot(feedback_model)
        pass
    ##########################################################################
    data = np.array([train_df.iloc[-13:].values])
    # print(data)
    # inputs, labels = multi_window.example
    # print(inputs)
    np_train_mean=train_mean.values
    np_train_std = train_std.values
    predict_result = multi_dense_model(data).numpy()*np_train_std+np_train_mean
    predict_result = pd.DataFrame(predict_result[0])
    predict_result.columns = train_df.columns
    # result = pd.DataFrame(predict_result)
    # print(predict_result)
    output_path = os.path.join(setting.DATA_ROOT_,file_name)+'_output.csv'
    predict_result.to_csv(output_path, index = False)
    # print(data)
    # data = np.array(data, dtype=np.float32)
    # print(multi_window.example)


# ########################################################################
'''


# #################################################################

# #################################################################



x = np.arange(len(multi_performance))
width = 0.3


metric_name = 'mean_absolute_error'
metric_index = multi_lstm_model.metrics_names.index('mean_absolute_error')
print(metric_index)
val_mae = [v[metric_index] for v in multi_val_performance.values()]
test_mae = [v[metric_index] for v in multi_performance.values()]

plt.bar(x - 0.17, val_mae, width, label='Validation')
plt.bar(x + 0.17, test_mae, width, label='Test')
plt.xticks(ticks=x, labels=multi_performance.keys(),
           rotation=45)
plt.ylabel(f'MAE (average over all times and outputs)')
_ = plt.legend()
plt.show()
for name, value in multi_performance.items():
  print(f'{name:8s}: {value[1]:0.4f}')

'''