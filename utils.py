import os
import re
import datetime
import setting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
# import tensorflow as tf

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PolynomialFeatures
pd.options.mode.chained_assignment = None


def read_all_csv():
    tables = {}
    directory = os.path.join(os.getcwd(), setting.DATA_ROOT_)
    for file_name in os.listdir(directory):
        assert file_name.endswith('.csv')
        tables[file_name] = pd.read_csv(os.path.join(directory, file_name))
    return tables

def data_cleasing(tables, species):
    assert not check_has_null(tables[species]).any()
    tables[species].trade_amount = remove_commas(
        tables[species].trade_amount)
    tables[species].trade_amount = invert_to_int(
        tables[species].trade_amount)
    tables[species].price_max = remove_commas(
        tables[species].price_max)
    tables[species].price_max = invert_to_int(
        tables[species].price_max)
    tables[species].leftover = remove_commas(
        tables[species].leftover)
    tables[species].leftover = invert_to_int(
        tables[species].leftover)
    tables[species].price_compare = remove_dash(
        tables[species].price_compare)
    tables[species].price_compare = invert_to_int(
        tables[species].price_compare)
    tables[species].trade_compare = remove_dash(
        tables[species].trade_compare)
    tables[species].trade_compare = invert_to_int(
        tables[species].trade_compare)
    return tables

def replace_year(df):
    for i in range(df.shape[0]):
        old = re.findall(r'(^\d+)/.+',df[i])[0]
        new = str(int(old)+1911)
        df[i]=re.sub(r'(^\d+)(/.+)', new+'\\2', df[i])
    return df

def CNY_datetime():
    chinese_new_year = ['2011/02/03','2012/01/23','2013/02/10','2014/01/31','2015/02/19','2016/02/08','2017/01/28','2018/02/16','2019/02/05','2020/01/25','2021/2/12']
    CNY_timestamp = pd.to_datetime(pd.Series(chinese_new_year), format='%Y/%m/%d') 
    CNY_timestamp = CNY_timestamp.map(datetime.datetime.timestamp)
    return CNY_timestamp

def date_to_period(timestamp_s):
    CNY_timestamp = CNY_datetime()
    year_period = []
    day = 24*60*60
    for i in range(timestamp_s.shape[0]):
        num = min(abs(timestamp_s[i]-CNY_timestamp))/day
        year_period.append(num)
    df = pd.DataFrame(year_period, columns=['year_period'])
    return df

def fft_datetime(df):
    fft = tf.signal.rfft(df)
    f_per_dataset = np.arange(0, len(fft))

    n_samples_h = len(df)
    hours_per_year = 24*354.3667
    years_per_dataset = n_samples_h/(hours_per_year)

    f_per_year = f_per_dataset/years_per_dataset
    plt.step(f_per_year, np.abs(fft))
    plt.xscale('log')
    plt.ylim(0, 400000)
    plt.xlim([0.1, max(plt.xlim())])
    plt.xticks([1, 354.3667], labels=['1/Year', '1/day'])
    _ = plt.xlabel('Frequency (log scale)')
    plt.show()

def invert_to_int(df):
    df = df.astype(int)
    return df

def remove_commas(df):
    for i in range(df.shape[0]):
        if isinstance(df.iloc[i], str):
            df.iloc[i] = re.sub(r'(?<=\d)[,]', '', df.iloc[i])
    return df

def remove_dash(df):
    for i in range(df.shape[0]):
        if isinstance(df.iloc[i], str):
            df.iloc[i] = re.sub(r'(\s+\-\s+)', '0', df.iloc[i])
    return df

def check_has_null(df):
    return df.isnull().any()

def data_cor_heatmap(pd_table):
    tomorrow_trade = pd_table['trade_amount'].values
    tomorrow_trade = tomorrow_trade[1:]
    pd_table = pd_table.iloc[:-1]
    pd_table['tomorrow'] = tomorrow_trade
    corr_matrix = pd_table.corr()
    fig, ax = plt.subplots()
    fig.set_size_inches(14, 10)
    ax = sns.heatmap(corr_matrix)
    plt.show()
    return

def data_visualize(df):
    stride = 10
    columns = df.columns
    t = df[columns[0]].iloc[::stride]
    s1 = df[columns[1]].iloc[::stride]
    s2 = df[columns[2]].iloc[::stride]
    s3 = df[columns[3]].iloc[::stride]

    ax1 = plt.subplot(311)
    plt.plot(t, s1)
    ax1.set_ylim([np.min(s1), np.max(s1)])
    plt.setp(ax1.get_xticklabels(), visible=False)
    # share x only
    ax2 = plt.subplot(312, sharex=ax1)
    ax2.set_ylim([np.min(s2), np.max(s2)])
    plt.plot(t, s2)
    # make these tick labels invisible
    plt.setp(ax2.get_xticklabels(), visible=False)

    # share x and y
    ax3 = plt.subplot(313, sharex=ax1)
    ax3.set_ylim([np.min(s3), np.max(s3)])
    plt.setp(ax3.get_xticklabels(), fontsize=6)
    plt.plot(t, s3)

    plt.show()
    return

def data_split(data, ratio):
    # 2/3 資料為訓練資料， 1/3 資料為測試資料
    train_size = int(len(data) * ratio)
    # print(train_size)
    test_size = len(data) - train_size
    # print(test_size)
    train, test = data[0:train_size, :], data[train_size:len(data), :]
    return train, test

def data_preparation_linear(data, look_back=setting.lookback_):
    dataX, dataY = [], []
    for i in range(data.shape[0]-look_back):
        a = data[i:(i+look_back), :]
        a = a.flatten()
        dataX.append(a)
        dataY.append(data[i + look_back, -1])
    return np.array(dataX), np.array(dataY)

def data_scale(data):
    scaler = MinMaxScaler(feature_range=(0, 1))
    data = scaler.fit_transform(data)
    return scaler, data

def data_poly(data, order=2):
    poly = PolynomialFeatures(order)
    poly_x = poly.fit_transform(data)
    return poly_x

def model_result_visualize(ground_true, validation, test, lookback=setting.lookback_):
    assert len(ground_true)-2*lookback == len(validation)+len(test)
    # plot baseline and predictions
    validationPlot = np.empty_like(ground_true)
    validationPlot[:] = np.nan
    validationPlot[lookback:len(validation)+lookback] = validation
    plt.plot(ground_true)
    plt.plot(validationPlot)

    testPlot = np.empty_like(ground_true)
    testPlot[:] = np.nan
    testPlot[len(validation)+(2*lookback):len(ground_true)] = test

    plt.plot(testPlot)
    plt.show()

def MAPE(actual_y, predict_y):
    idx = np.where(actual_y == 0)
    new_actual_y = np.delete(actual_y, idx)
    new_predict_y = np.delete(predict_y, idx)
    return np.mean(np.abs((new_actual_y-new_predict_y)/new_actual_y))

def generate_upload_using_data(data):
    return data[-13:]

def generate_upload_result(table, data, species, column, model, scale_target):
    if species not in table.keys():
        table[species] = {}

    predict = model.predict(data)
    predict = predict/scale_target
    assert predict.shape[0] == 13
    table[species][column] = predict

    return table

def generate_upload_file(tables, upload_table):
    
    t = tables['test_lily_price.csv']
    for species in upload_table:
        for column in upload_table[species]:
            idx = get_test_lily_price_idx(t, species, column)
            if column == 'trade_amount':
                t['volume'][idx]=upload_table[species][column]
            else:
                t[column][idx]=upload_table[species][column]
    return t
    
def get_test_lily_price_idx(df, species, column):
    if column == 'trade_amount':
        column = 'volume'
    idx = df[column][df['flower_no']==species[:5]].index
    return idx[:13]

def cleanse_weather_by_duplicate(df):
    for column in df.columns:
        prev_value = df[column].iloc[0]
        for i in range(1,len(df[column])):
            if df[column].iloc[i] in ['...','/','X']:
                df[column].iloc[i] = prev_value
            prev_value = df[column].iloc[i]
    return df

def change_to_dummy(df):
    for i in range(len(df)):
        if df.iloc[i] != 0:
            df.iloc[i] = 1
    return df

