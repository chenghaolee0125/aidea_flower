import os
import time
import datetime

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf

from setting import *
from utils import *
from Model import Baseline, compile_and_fit, ResidualWrapper, FeedBack
from Generator import WindowGenerator

val_performance = {}
performance = {}
volume_MAPE = []
# file_name = file_names[0]
for i in range(len(file_names_)):

    print(file_names_[i])
    df = pd.read_csv(os.path.join(ClEANSE_DATA_ROOT_,file_names_[i])+'.csv')
    df = df.drop(columns=['date'])

    idx_non_zero = df['trade_amount']==0
    idx_non_zero = [i for i, x in enumerate(idx_non_zero) if x]
    df = df.drop(idx_non_zero)
    df['trade_amount'] = np.log(df['trade_amount'])


    n = len(df)
    train_df = df[int(n*0.3):-13]
    val_df = df[int(n*0.1):int(n*0.3)]
    test_df = df[0:int(n*0.1)]

    num_features = df.shape[1]

    train_mean = train_df.mean()
    # print(train_mean)
    train_std = train_df.std()

    train_df = (train_df - train_mean) / train_std
    val_df = (val_df - train_mean) / train_std
    test_df = (test_df - train_mean) / train_std

    OUT_STEPS = OUT_STEPS_
    IN_STEPS = IN_STEPS_
    output_column = output_columns_[0]
    # for output_column in output_columns:

    wide_window = WindowGenerator(
        input_width=IN_STEPS, label_width=OUT_STEPS, shift=OUT_STEPS,
        train_df=train_df, val_df=val_df, test_df=test_df,
        label_columns=[output_column])

    linear = tf.keras.Sequential([
        tf.keras.layers.Flatten(),
        # tf.keras.layers.Dropout(.01),
        # tf.keras.layers.Dense(OUT_STEPS, 
        #             kernel_initializer=tf.initializers.zeros,
        #             activation='relu'),
        tf.keras.layers.Dense(OUT_STEPS,
                    kernel_initializer=tf.initializers.zeros)
    ])

    linear_base = tf.keras.Sequential([
        tf.keras.layers.Dense(units=1)
    ])

    lstm_model = tf.keras.models.Sequential([
        # Shape [batch, time, features] => [batch, time, lstm_units]
        tf.keras.layers.LSTM(2, return_sequences=True),
        # Shape => [batch, time, features]
        tf.keras.layers.Dense(units=1),
        tf.keras.layers.Reshape([1,-1])
    ])

    CONV_WIDTH = 3
    multi_conv_model = tf.keras.Sequential([
        # Shape [batch, time, features] => [batch, CONV_WIDTH, features]
        tf.keras.layers.Lambda(lambda x: x[:, -CONV_WIDTH:, :]),
        # Shape => [batch, 1, conv_units]
        tf.keras.layers.Conv1D(8, kernel_size=(CONV_WIDTH),activation='relu',kernel_regularizer='l1_l2'),
        # Shape => [batch, 1,  out_steps*features]
        tf.keras.layers.Dense(OUT_STEPS,
                        kernel_initializer=tf.initializers.zeros,
                        ),
        # Shape => [batch, out_steps, features]
    ])


    # print('Input shape:', wide_window.example[0].shape)
    history = compile_and_fit(multi_conv_model, wide_window)

    # val_performance['Linear'] = linear.evaluate(wide_window.val)
    # performance['Linear'] = linear.evaluate(wide_window.test, verbose=0)
    # print(performance)

    data = np.array([train_df.iloc[-(IN_STEPS):].values])
    np_train_mean=train_mean[output_column]
    np_train_std = train_std[output_column]

    predict_result = multi_conv_model(data).numpy()*np_train_std+np_train_mean
    predict_result = np.array(predict_result).flatten()
    if output_column == 'trade_amount':
        # print(predict_result)
        predict_result = np.exp(predict_result)
        predict_result = pd.DataFrame(predict_result,columns=['volume'])
    else:
        predict_result = pd.DataFrame(predict_result,columns=[output_column])

    groundtruth = pd.read_csv('data/groundtruth.csv')
    assert predict_result.shape[0] == OUT_STEPS
    print(predict_result)
    volume_MAPE.append(MAPE(groundtruth['volume'][idx_[i]].values,predict_result['volume'].values))
    print(volume_MAPE[-1])

print(volume_MAPE)
    # output_csv = pd.concat([output_csv,predict_result],axis=1)
    # print(output_csv)

    # output_path = os.path.join(DATA_ROOT_,file_name)+'_output.csv'
    # output_csv.to_csv(output_path, index = False)
    



    # predict_result.columns = train_df.columns
    # # result = pd.DataFrame(predict_result)
    # # print(predict_result)
