# Experiment Pipeline

## LSTM ##
There is one day lagging in the original version (look_back = 1). According to my friend (one of the best NLP experts I have ever known), It is because MSE loss function tends to find the local min at the previous input value, which is the previous date. After my second thoughts, "look_back = 1" doesn't make any sense. LSTM model just has only one input to generate one output. WTF. The original author get busted.  
```python
def create_dataset(dataset, look_back=1):
	dataX, dataY = [], []
	for i in range(len(dataset)-look_back-1):
		a = dataset[i:(i+look_back), 0]
		dataX.append(a)
		dataY.append(dataset[i + look_back, 0])
	return numpy.array(dataX), numpy.array(dataY)

look_back = 1
trainX, trainY = create_dataset(train, look_back)
testX, testY = create_dataset(test, look_back)
```
![snip](../src/lookback_1.JPG)

Anyway, I change the look_back value to 9. It seems that the lagging problem still exists and it looks like being applied low pass filter.
![snip](../src/lookback_9.JPG)

Finally, I tried to not use ground truth value to predict during testing process.  