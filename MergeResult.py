import os
from utils import *
from setting import *
import pandas as pd
import numpy as np

output = pd.read_csv('data/output.csv')


tables = {}
for file_name in file_names_:
    directory = os.path.join(TEMP_DATA_ROOT_,file_name)
    tables[file_name] = pd.read_csv(directory+'_output.csv')

    idx = output[output['flower_no']==file_name].index
    idx = idx[-13:]

    for output_column in output_columns_:
        if output_column == 'trade_amount':
            output['volume'][idx]=tables[file_name]['volume']
        else:    
            output[output_column][idx]=tables[file_name][output_column]

output.to_csv(FILE_ROOT_, index = False)


groundtruth = pd.read_csv('data/groundtruth.csv')
volume_MAPE = MAPE(groundtruth['volume'].values,output['volume'].values)
price_high_MAPE = MAPE(groundtruth['price_high'].values,output['price_high'].values)
price_mid_MAPE = MAPE(groundtruth['price_mid'].values,output['price_mid'].values)
price_avg_MAPE = MAPE(groundtruth['price_avg'].values,output['price_avg'].values)
print('price_high_MAPE = {}'.format(price_high_MAPE))
print('price_mid_MAPE = {}'.format(price_mid_MAPE))
print('price_avg_MAPE = {}'.format(price_avg_MAPE))
print('volume_MAPE = {}'.format(volume_MAPE))
print('Total MAPE = {}'.format(0.1*price_high_MAPE+0.15*price_mid_MAPE+0.25*price_avg_MAPE+0.5*volume_MAPE))
# print(output[:20])
# print(tables)


# print(idx)
