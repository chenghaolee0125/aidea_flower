import tensorflow as tf
from setting import *
import pandas as pd

OUT_STEPS = OUT_STEPS_
IN_STEPS = IN_STEPS_
num_features = pd.read_csv(ClEANSE_DATA_ROOT_+'/FS443.csv').shape[1] - 1

linear = tf.keras.Sequential([
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dropout(.1),

    tf.keras.layers.Dense(OUT_STEPS, 
                kernel_initializer=tf.initializers.zeros,
                activation='relu'),
    tf.keras.layers.Dense(OUT_STEPS,
                kernel_initializer=tf.initializers.zeros)
])

linear_base = tf.keras.Sequential([
    tf.keras.layers.Dense(units=1)
])

lstm_model = tf.keras.models.Sequential([
    # Shape [batch, time, features] => [batch, time, lstm_units]
    tf.keras.layers.LSTM(2, return_sequences=True),
    # Shape => [batch, time, features]
    tf.keras.layers.Dense(units=1),
    tf.keras.layers.Reshape([1,-1])
])


CONV_WIDTH = 3
# print((1,IN_STEPS,num_features))
multi_conv_model = tf.keras.Sequential([
    # Shape [batch, time, features] 
    # Shape => [batch, 1, conv_units]
    # tf.keras.layers.Conv1D(4, activation='relu', kernel_size=(CONV_WIDTH)),
    tf.keras.layers.Conv1D(1, activation='relu', kernel_size=(CONV_WIDTH)),
    tf.keras.layers.Flatten(),

    # tf.keras.layers.Conv1D(4, activation='relu', kernel_size=(CONV_WIDTH)),
    # Shape => [batch, 1,  out_steps*features]
    tf.keras.layers.Dense(OUT_STEPS, 
                        kernel_initializer=tf.initializers.zeros),
    # Shape => [batch, out_steps, features]
])



multi_conv_model.build((1,IN_STEPS,num_features))
multi_conv_model.summary()
# tf.keras.utils.plot_model(linear, "my_first_model_with_shape_info.png", show_shapes=True)

# linear_base.build((1,IN_STEPS,num_features))
# linear_base.summary()