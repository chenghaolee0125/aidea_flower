import numpy as np
# MEDIA
DATA_ROOT_ = "data/Preprocess_v2"
ClEANSE_DATA_ROOT_ = "data/Cleanse"
TEMP_DATA_ROOT_ = "data/Temp_output"
FILE_ROOT_ = "data/output.csv"
WEATHER_ROOT_ = "crawler/weather.csv"
SAVE_MODEL_ROOT_ = "SaveModel/"
# time series
lookback_ = 13

OUT_STEPS_ = 13
IN_STEPS_ = 6

# linear
order_ = 1

file_names_ = ['FS443','FS479','FS592','FS609','FS639','FS779','FS859','FS879','FS899','FS929']
output_columns_ = ['trade_amount','price_high','price_mid','price_avg']
chinese_new_year_ = ['2011/02/03','2012/01/23','2013/02/10','2014/01/31',
                '2015/02/19','2016/02/08','2017/01/28','2018/02/16','2019/02/05'
                ,'2020/01/25','2021/2/12']

idx_ = np.array(list(range(0, 130))).reshape(13,10).T

train_times_ = 3