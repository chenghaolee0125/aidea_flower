from bs4 import BeautifulSoup, NavigableString
import re


def get_date(soup):
    tbchild = soup.find('tbody').children
    for td in tbchild:
        if isinstance(td, NavigableString):
            continue
        table_time = re.findall(r'觀測時間:(.+)',td.text)
        if table_time:
            break
    return table_time

def get_column_name(soup):
    trs = soup.find('tr',class_="third_tr")
    column_name = list()
    for tr in trs:
        buff = re.findall(r'<th>(.+)</th>',str(tr))
        if buff:
            column_name.append(buff[0])
    return column_name

def get_context(soup):
    trs = soup.find_all('tr')
    rows = list()
    for tr in trs:
        rows.append([td.text.replace('\n', '').replace('\xa0', '') for td in tr.find_all('td')])
    rows = rows[4:]
    return rows
    # print(row[0])


    


