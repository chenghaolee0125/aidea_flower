# coding: utf-8
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys

import time
import datetime
import pandas as pd

from crawler_utils import get_date, get_column_name, get_context
from crawler_setting import *


#啟動模擬瀏覽器
driver = webdriver.Firefox()
#取得網頁代馬
driver.get(url_)

time.sleep(1)


County = Select(driver.find_element_by_id("stationCounty"))
County.select_by_value("臺北市")

station = Select(driver.find_element_by_id("station"))
station.select_by_visible_text("內湖 (Neihu)")

datatype = Select(driver.find_element_by_id("datatype"))
datatype.select_by_value("mn")

datepicker  = driver.find_element_by_id("datepicker")
datepicker.send_keys(startdate_)
doquery  = driver.find_element_by_id("doquery").click()

time.sleep(1)

df_from_startdate_to_enddate = pd.DataFrame([])


while startdate_ != enddate_:
    driver.switch_to.window(driver.window_handles[-1])
    soup = BeautifulSoup(driver.page_source, features='lxml')

    year_month = get_date(soup)
    column_name = get_column_name(soup)
    contexts = get_context(soup)

    assert len(column_name) == len(contexts[0])

    df_month = pd.DataFrame(contexts,columns=column_name)
    df_month['ObsTime'] = [year_month[0]+'-']+df_month['ObsTime']
    df_from_startdate_to_enddate = pd.concat([df_from_startdate_to_enddate,df_month],axis=0)
    # print(df_month)
    print(year_month)
    if year_month[0] != enddate_:
        driver.find_element_by_id("nexItem").click()
        driver.get(driver.current_url)
    else:
        # print('break')
        break
    time.sleep(1)

# print(df_from_startdate_to_enddate)
df_from_startdate_to_enddate.to_csv(output_path_, index = False)


# driver.quit()
