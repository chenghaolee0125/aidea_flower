import os
import time
import datetime

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf

from setting import *
from utils import *
from Model import Baseline, compile_and_fit, ResidualWrapper, FeedBack
from Generator import WindowGenerator

val_performance = {}
performance = {}

for i in range(len(file_names_)):

    print(file_names_[i])

    df = pd.read_csv(os.path.join(ClEANSE_DATA_ROOT_,file_names_[i])+'.csv')
    df = df.drop(columns=['date','Temperature','T Max','T Min','RH','RHMin','Precp'])
    # df['leftover'] = change_to_dummy(df['leftover'])
    # df['Precp'] = change_to_dummy(df['Precp'])

    idx_non_zero = df['trade_amount']==0
    idx_non_zero = [i for i, x in enumerate(idx_non_zero) if x]
    df = df.drop(idx_non_zero)

    n = len(df)
    data = df.iloc[-(IN_STEPS_):]
    print(data)
    train_df = df.iloc[int(n*0.3):]
    val_df = df.iloc[int(n*0.1):int(n*0.3)]
    test_df = df.iloc[0:int(n*0.1)]

    num_features = df.shape[1]

    train_mean = train_df.mean()
    train_std = train_df.std()

    train_df = (train_df - train_mean) / train_std
    val_df = (val_df - train_mean) / train_std
    test_df = (test_df - train_mean) / train_std

    OUT_STEPS = OUT_STEPS_
    IN_STEPS = IN_STEPS_

    output_csv = pd.DataFrame([])

    best_loss_value = 100

    for output_column in output_columns_:
        wide_window = WindowGenerator(
            input_width=IN_STEPS, label_width=OUT_STEPS, shift=OUT_STEPS,
            train_df=train_df, val_df=val_df, test_df=test_df,
            label_columns=[output_column])

        # for train_time in range(train_times_):
        #     CONV_WIDTH = 3
        #     # print((1,IN_STEPS,num_features))
        #     multi_conv_model = tf.keras.Sequential([
        #         # Shape [batch, time, features] 
        #         # Shape => [batch, 1, conv_units]
        #         # tf.keras.layers.Conv1D(4, activation='relu', kernel_size=(CONV_WIDTH)),
        #         tf.keras.layers.Conv1D(1, activation='relu', kernel_size=(CONV_WIDTH)),
        #         tf.keras.layers.Flatten(),
        #         # tf.keras.layers.Conv1D(4, activation='relu', kernel_size=(CONV_WIDTH)),
        #         # Shape => [batch, 1,  out_steps*features]
        #         tf.keras.layers.Dense(OUT_STEPS, 
        #                             kernel_initializer=tf.initializers.zeros()),
        #         # Shape => [batch, out_steps, features]
        #     ])


        #     # print('Input shape:', wide_window.example[0].shape)
        #     history = compile_and_fit(multi_conv_model, wide_window)

        #     # val_performance['conv'] = multi_conv_model.evaluate(wide_window.val)
        #     # performance['conv'] = multi_conv_model.evaluate(wide_window.test, verbose=0)
        #     # print(performance)
        #     buff_loss_value = multi_conv_model.evaluate(wide_window.test, verbose=0)[0]
        #     if buff_loss_value < best_loss_value:
        #         best_loss_value = buff_loss_value
        #         multi_conv_model.save(SAVE_MODEL_ROOT_+file_names_[i]+output_column+'.h5')
            

        multi_conv_model = tf.keras.models.load_model(SAVE_MODEL_ROOT_+file_names_[i]+output_column+'.h5')
        
        data = np.array([train_df.iloc[-(IN_STEPS):].values])
        
        np_train_mean=train_mean[output_column]
        np_train_std = train_std[output_column]   
        print(data*np_train_std+np_train_mean)
        predict_result = multi_conv_model(data).numpy()*np_train_std+np_train_mean
        
        predict_result = np.array(predict_result).flatten()
        print(predict_result)
        if output_column == 'trade_amount':
            predict_result = pd.DataFrame(predict_result,columns=['volume'])
        else:
            predict_result = pd.DataFrame(predict_result,columns=[output_column])
        output_csv = pd.concat([output_csv,predict_result],axis=1)


    output_path = os.path.join(TEMP_DATA_ROOT_,file_names_[i])+'_output.csv'
    output_csv.to_csv(output_path, index = False)
    