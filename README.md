# Project Description #  
https://aidea-web.tw/topic/4c321260-fe3b-4b68-ab42-c1e8f86553ef  

運用爬蟲與資料科學預測香水百合花卉各項市場數值  
regard more info below

# Timeline
| time    | event |
|------------|------|
| 2020/11/25 | 	報名開始 |
| 2020/12/09 | 開放上傳  |
| 2021/01/27 23:59:59  | 上傳截止 |
| 2021/02/23  | 公布Private Leaderboard |
| 2021/03/02 23:59:59    | 繳交報告截止 |
|2021/03/05 | 公布得獎名單 |

# Data #
**農產品批發市場交易行情站**  
https://amis.afa.gov.tw/  

![snip](/src/snip.JPG)


```
下載說明:  
花卉交易資料可至網站之「花卉行情 > 產品日交易行情」下載：  
範圍別： 如果選取「期間」，請勿一次設定太長期間，否則無法下載。  
產品代號： 可輸入1~5碼任意長度，例如輸入FS 可下載所有香水百合類的交易行情資料。  
市場： 可查詢下載全臺五個花卉批發市場的價格，本議題Private Leaderboard正確答案以「105台北花市」為準  
```

資料欄位說明:
* 最高價（元/把）：當日該農產品成交之每把最高價。
* 上價（元/把）：以當日該農產品總交易量中最高價格之20％，加權平均計算得之。
* 下價（元/把）：以當日該農產品總交易量中最低價格之20％，加權平均計算得之。
* 中價（元/把）：以當日該農產品總交易量中扣除最高最低價格各20％剩餘之60％，加權平均計算得之。
* 平均價（元/把）：當日該農產品總成交之每把平均價。
* 交易量（把）：當日該農產品總成交量。
* 殘貨量（把）：係指花卉經線上拍賣二次未能於底價（50元）成交，即為殘貨（流標），該產品即直接下架，並於拍賣後統一銷毀，不得再轉以其他交易形式銷售。

# Evaluation #
![eqa](/src/CodeCogsEqn.png)

| feature    | weight|
|------------|------|
| price_high | 0.1  |
| price_mid  | 0.15 |
| price_avg  | 0.25 |
| volume     | 0.5  |

# Note #  
- [x] problem understanding
- [ ] domain knowledge preparation
- [ ] web crawler
- [ ] data cleasing
- [ ] data transform (optional)
- [ ] Modeling
- [ ] Validation
- [ ] Deployment (up to 3 times per day)

# Reference #
Baseline model: MAPE 7.64%  
http://etds.lib.tku.edu.tw/etdservice/view_metadata?etdun=U0002-0907201822040300&from=DEPT&deptid=D0002009027

NY Stock Price Prediction RNN LSTM GRU  
https://www.kaggle.com/raoulma/ny-stock-price-prediction-rnn-lstm-gru

To Do: 
https://www.tensorflow.org/tutorials/structured_data/time_series  

Deal with periodicity of date time using cos and sin (lunar calendar)  
use tf.signal.rfft to find approapriate freqs  

Try one-shot and autoregressive

Maybe use moving avg to normalize data, otherwise use z score normalization.  
