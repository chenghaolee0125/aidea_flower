import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from utils import *
from setting import *


if __name__ == "__main__":

    df = pd.read_csv('data/Cleanse/FS443.csv')
    data_cor_heatmap(df)
    # print(df.info())

    df = df.drop(columns=['date'])

    df_leftover_dummy = change_to_dummy(df['leftover'])
    df_precp_dummy = change_to_dummy(df['Precp'])
    
    # print(df.head())
    idx_non_zero = df['trade_amount']==0
    idx_non_zero = [i for i, x in enumerate(idx_non_zero) if x]
    df = df.drop(idx_non_zero)
    df['trade_amount'] = np.log(df['trade_amount'])
    
    print(df['trade_amount'])

    n = len(df)
    train_df = df[int(n*0.3):-13]
    val_df = df[int(n*0.1):int(n*0.3)]
    test_df = df[0:int(n*0.1)]

    num_features = df.shape[1]


    train_mean = train_df.mean()
    train_std = train_df.std()

    train_df = (train_df - train_mean) / train_std
    val_df = (val_df - train_mean) / train_std
    test_df = (test_df - train_mean) / train_std

    for column in df.columns:
        print(column)
        # ax = train_df[column].plot.hist(bins=25, alpha=0.5)
        # print(train_df.head())
        ax = train_df.plot.scatter(x='trade_amount', y=column)
        plt.show()
        
    
