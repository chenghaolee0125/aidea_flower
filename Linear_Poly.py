import pandas as pd
import sklearn
import setting
from utils import *
from Model import modeling
from sklearn.metrics import mean_absolute_error

tables = read_all_csv()

species_list = ['FS443.csv', 'FS479.csv', 'FS592.csv', 'FS609.csv', 'FS639.csv',
                'FS779.csv', 'FS859.csv', 'FS879.csv', 'FS899.csv', 'FS929.csv']
variables_list = [['price_max', 'price_high', 'price_mid', 'price_low', 'price_avg', 'price_compare', 'trade_compare', 'leftover', 'trade_amount'], ['price_max', 'trade_amount', 'price_mid', 'price_low',
                'price_avg', 'price_compare', 'trade_compare', 'leftover', 'price_high'], ['price_max', 'price_high', 'trade_amount', 'price_low',
                'price_avg', 'price_compare', 'trade_compare', 'leftover', 'price_mid'], ['price_max', 'price_high', 'price_mid', 'price_low',
                'trade_amount', 'price_compare', 'trade_compare', 'leftover', 'price_avg']]


for species in species_list:
    print(species)
    tables = data_cleasing(tables, species)
    # print(tables[species])
# data_visualize(tables['FS443.csv'])

upload_table = {}

for species in species_list:
    for variables in variables_list:
        print(species, variables[-1])
        cleased_data = tables[species][variables]

        # data_cor_heatmap(cleased_data)

        # normalize_data = np.array
        scale, normalize_data = data_scale(cleased_data.values)
        scale_target = scale.scale_[-1]

        train, test = data_split(normalize_data, 0.67)
        # print("test.shape = {}".format(test.shape))
        train_X, train_Y = data_preparation_linear(
            train, look_back=setting.lookback_)
        test_X, test_Y = data_preparation_linear(
            test, look_back=setting.lookback_)

        train_X = data_poly(train_X, order=setting.order_)
        linear = modeling(train_X, train_Y)
        reg, score = linear.linearmodel()
        predict_train_x = reg.predict(train_X)
        print("train R2 = {}".format(score))
        # print("test_X.shape = {}".format(test_X.shape))
        test_X = data_poly(test_X, order=setting.order_)
        # print("test_X.shape = {}".format(test_X.shape))
        predict_test_x = reg.predict(test_X)

        '''
        model_result_visualize(normalize_data[:, -1]/scale_target,
                               predict_train_x/scale_target, predict_test_x/scale_target)
        '''
        print("MAPE= {}".format(
            MAPE(test_Y/scale_target, predict_test_x/scale_target)))
        # print(type(test_Y))
        upload_test_X = generate_upload_using_data(test_X)
        # print(upload_test_X.shape)

        upload_table = generate_upload_result(
            upload_table, upload_test_X, species, variables[-1], reg, scale_target)
        # print(upload_table)
        # print(generate_upload_using_data(test_Y)/scale_target)
        print("MAPE= {}".format(MAPE(generate_upload_using_data(
            test_Y)/scale_target, upload_table[species][variables[-1]])))

print(upload_table)

# t = generate_upload_file(tables, upload_table)
# t.to_csv(setting.FILE_ROOT_, index = False)