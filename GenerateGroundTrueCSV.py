import os
from utils import *
import pandas as pd
import numpy as np

file_names = ['FS443','FS479','FS592','FS609','FS639','FS779','FS859','FS879','FS899','FS929']
output_columns = ['price_high','price_mid','price_avg','volume']

groundtruth = pd.read_csv('data/groundtruth.csv')

tables = {}
for file_name in file_names:
    directory = os.path.join(setting.DATA_ROOT_,file_name)
    tables[file_name] = pd.read_csv(directory+'.csv')[-13:]


    cleasing_commas_cols = ['trade_amount', 'price_max', 'leftover']
    for col in cleasing_commas_cols:
        tables[file_name][col] = remove_commas(tables[file_name][col])
        tables[file_name][col] = invert_to_int(tables[file_name][col])
    cleasing_dash_cols = ['price_compare','trade_compare']
    for col in cleasing_dash_cols:
        tables[file_name][col] = remove_dash(tables[file_name][col])
        tables[file_name][col] = invert_to_int(tables[file_name][col])


    idx = groundtruth[groundtruth['flower_no']==file_name].index
    idx = idx[-13:]

    for output_column in output_columns:
        if output_column == 'volume':
            groundtruth[output_column][idx]=tables[file_name]['trade_amount']
            print(groundtruth[output_column][idx])
        else:    
            groundtruth[output_column][idx]=tables[file_name][output_column]

groundtruth.to_csv('data/groundtruth.csv', index = False)
print(groundtruth[:20])
# print(tables)



# print(idx)
