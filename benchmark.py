import tensorflow as tf
import time
import matplotlib.pyplot as plt

def performanceTest(device_name,size):
    tf.debugging.set_log_device_placement(True)
    startTime = time.time()
    with tf.device(device_name):
        W = tf.random.normal([size, size], name='W')
        X = tf.random.normal([size, size], name='X')
        mul = tf.matmul(W, X, name='mul')
        sum_result = tf.reduce_sum(mul, name='sum')
        
    print(sum_result)

    takeTimes = time.time() - startTime
    print(device_name, "矩陣大小：",size,"x",size, " 時間:",takeTimes)
    return takeTimes


gpu_set = [];cpu_set = [];i_set = []
for i in range(1, 10001, 500):
    g = performanceTest("/gpu:0",i)
    c = performanceTest("/cpu:0",i)
    gpu_set.append(g)
    cpu_set.append(c)
    i_set.append(i)

# fig = plt.gcf()
plt.plot(i_set, gpu_set, label='gpu')
plt.plot(i_set, cpu_set, label='cpu')
plt.legend()
plt.show()