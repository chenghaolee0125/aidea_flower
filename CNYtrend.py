import os
import setting
from utils import *
import pandas as pd
import matplotlib.pyplot as plt

CNY_datetime = pd.to_datetime(setting.chinese_new_year, format='%Y/%m/%d')
output_species = {}
output_weekday = {}
for i in range(len(setting.file_names)):
    
    # directory = os.path.join(setting.DATA_ROOT_,file_name)
    # tables[file_name] = pd.read_csv(directory+'_output.csv')

    print(setting.file_names[i])
    df = pd.read_csv(os.path.join(setting.DATA_ROOT_,setting.file_names[i])+'.csv')
    # print(df.head())

    cleasing_commas_cols = ['trade_amount', 'price_max', 'leftover']
    for col in cleasing_commas_cols:
        df[col] = remove_commas(df[col])
        df[col] = invert_to_int(df[col])
    cleasing_dash_cols = ['price_compare','trade_compare']
    for col in cleasing_dash_cols:
        df[col] = remove_dash(df[col])
        df[col] = invert_to_int(df[col])
    df_volume = df['trade_amount']
    # data_visualize(df)

    df_date = df.pop('date')
    df_date = replace_year(df_date)
    # print(df_date)
    date_time = pd.to_datetime(df_date, format='%Y/%m/%d')
    # print(date_time.head())
    each_year_idx = 0
    roi = 13
    output_species[setting.file_names[i]] = pd.DataFrame()
    output_weekday[setting.file_names[i]+'_weekday'] = pd.DataFrame()
    each_date_idx = 0
    while each_date_idx < len(date_time):
        # print(each_date_idx)
        
        date_dif = (date_time[each_date_idx]-CNY_datetime[each_year_idx]).days
        
        if each_year_idx >= len(CNY_datetime):
            break
        if date_dif >=0:
            each_year_idx += 1
            continue
        elif date_dif == -1:
            buff = df_volume.iloc[(each_date_idx-(roi-1)):(each_date_idx+1)].values
            # if the date before new year doesn't have enough date of roi, then skip
            if buff.shape[0] != roi:
                each_year_idx += 1
                continue
            buff = pd.DataFrame(buff, columns=[setting.chinese_new_year[each_year_idx]])
            weekday = date_time.iloc[(each_date_idx-(roi-1)):(each_date_idx+1)].dt.dayofweek.values
            weekday = pd.DataFrame(weekday, columns=[setting.chinese_new_year[each_year_idx]])
            output_weekday[setting.file_names[i]+'_weekday']=pd.concat([output_weekday[setting.file_names[i]+'_weekday'],weekday],axis=1) 
            # print(weekday)
            output_species[setting.file_names[i]]=pd.concat([output_species[setting.file_names[i]],buff],axis=1)   
            each_year_idx += 1
        each_date_idx += 1
    output_species[setting.file_names[i]] = output_species[setting.file_names[i]].drop(columns=['2020/01/25'])
    # print(output_species[setting.file_names[i]])
    # print(output_weekday[setting.file_names[i]+'_weekday'])
    
    directory = os.path.join(setting.DATA_ROOT_,setting.file_names[i])
    species_output = pd.read_csv(directory+'_output.csv')
    species_output['volume']=output_species[setting.file_names[i]].quantile(q=0.15,axis=1)
    output_path = os.path.join(setting.DATA_ROOT_,setting.file_names[i])+'_output.csv'
    species_output.to_csv(output_path, index = False)


    output_species[setting.file_names[i]] = output_species[setting.file_names[i]].transpose()
    boxplot = output_species[setting.file_names[i]].boxplot()
    plt.show()
        
    
    